import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import css from '../../assets/css/my.css';

function Index() {
  return <h1>Home</h1>;
}

function About() {
  return <h2>About</h2>;
}

function Users() {
  return <h3>Users</h3>;
}

function Indexbody(){
  return <div>Indexbody</div>;
}
function Aboutbody(){
  return <div>Aboutbody</div>;
}
function Usersbody(){
  return <div>Usersbody</div>;
}

class AppRouter extends Component {
  render () {
    return (
    <Route>
    <div>

    <div id="title">
      <Route path="/" exact component={Index} />
      <Route path="/about/" component={About} />
      <Route path="/users/" component={Users} /> 
    </div>

    <div className="main-container">
        <div className="fixer-container">
          <nav>
            <ul className="list-of-floating-elements">
              <li className="floated">
                <Link to="/" className="aaa">1.Home</Link>
              </li>
              <li className="floated">
                <Link to="/about/" className="aaa">2.About</Link>
              </li>
              <li className="floated">
                <Link to="/users/" className="aaa">3.Users</Link>
              </li>
            </ul>
          </nav>
        </div>
    </div>

    <div id="body">
      <Route path="/" exact component={Indexbody} />
      <Route path="/about/" component={Aboutbody} />
      <Route path="/users/" component={Usersbody} /> 
    </div>

    </div>
  </Route>
    )
  }
}

export default AppRouter