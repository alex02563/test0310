import 'babel-polyfill'
import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter } from "react-router-dom";

import Main from './src/js/main.js'
import About from './src/about.jsx'

const root = document.getElementById('root')
const App=()=> (
    <BrowserRouter>
    <Main />
    <About/>
    </BrowserRouter>
  
)

ReactDOM.render(<App />, root);
